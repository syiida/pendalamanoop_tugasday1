<?php
require_once 'autoload.php';
class Harimau
{
    use Hewan, Fight;
    public function __construct($nama)
    {
        $this->set_nama($nama);
        $this->set_jumlahKaki(4);
        $this->set_keahlian('lari cepat');
        $this->set_attackPower(7);
        $this->set_deffencePower(8);
    }
    public function getInfoHewan()
    {
        $info = "Jenis Hewan : Elang<br>
        Nama Hewan : {$this->get_nama()}<br>
        Jumlah Kaki: {$this->get_jumlahKaki()}<br>
        Keahlian   : {$this->get_keahlian()}<br>
        Attack Power : {$this->get_attackPower()}<br>
        Deffence Power : {$this->get_deffencePower()}<br>
        Jumlah darah : {$this->get_darah()} <hr>";
        return $info;
    }
}
