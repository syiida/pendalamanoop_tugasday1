<?php
require_once 'autoload.php';
trait Hewan
{
    private $nama, $darah = 50, $jumlahKaki, $keahlian;
    //set method
    public function set_nama($nama)
    {
        $this->nama = $nama;
        return $this;
    }
    protected function set_darah($attackPowerPenyerang, $deffencePowerDiserang)
    {
        $this->darah = $this->darah - ($attackPowerPenyerang / $deffencePowerDiserang);
        return $this;
    }
    protected function set_jumlahKaki($kaki)
    {
        $this->jumlahKaki = $kaki;
        return $this;
    }
    protected function set_keahlian($keahlian)
    {
        $this->keahlian = $keahlian;
    }

    //get method
    public function get_nama()
    {
        return $this->nama;
    }
    public function get_darah()
    {
        return $this->darah;
    }
    public function get_jumlahKaki()
    {
        return $this->jumlahKaki;
    }
    public function get_keahlian()
    {
        return $this->keahlian;
    }
    //atraksi method
    public function atraksi()
    {
        return "{$this->nama} sedang {$this->keahlian} <br>";
    }
}
