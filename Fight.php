<?php
require_once 'autoload.php';
trait Fight
{
    use Hewan;
    private $attackPower, $deffencePower;
    //set method
    protected function set_attackPower($power)
    {
        $this->attackPower = $power;
        return $this;
    }
    protected function set_deffencePower($power)
    {
        $this->deffencePower = $power;
        return $this;
    }
    //get method
    public function get_attackPower()
    {
        return $this->attackPower;
    }
    public function get_deffencePower()
    {
        return $this->deffencePower;
    }
    //serang diserang method
    public function serang($penyerang, $diserang)
    {
        echo "{$penyerang} sedang menyerang {$diserang}<br>";
        $this->diserang($penyerang, $diserang);
    }
    public function diserang($penyerang, $diserang)
    {
        echo "{$diserang} sedang diserang <br>";
        if (substr_compare(strtolower($penyerang), 'elang', 1, 5, TRUE)) {
            $this->set_darah(10, 8);
        } else {
            $this->set_darah(7, 5);
        }
        echo "sisa darah {$diserang} :" . $this->get_darah() . "<hr>";
    }
}
